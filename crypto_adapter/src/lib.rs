use sodiumoxide::crypto::box_;
use sodiumoxide::crypto::sealedbox;
use sodiumoxide::crypto::sign;
use std::str;

type PubCryptoSecretKey = sodiumoxide::crypto::box_::SecretKey;
type PubCryptoPublicKey = sodiumoxide::crypto::box_::PublicKey;
type Nonce = sodiumoxide::crypto::box_::Nonce;

pub struct PubCryptoKeys {
    pub public: PubCryptoPublicKey,
    pub secret: PubCryptoSecretKey,
}

pub fn generate_keys() -> PubCryptoKeys {
    let (pub_key, sec_key) = box_::gen_keypair();
    PubCryptoKeys {
        public: pub_key,
        secret: sec_key,
    }
}

pub fn generate_nonce() -> Nonce {
    box_::gen_nonce()
}

pub struct PubCryptoKeysRef<'a> {
    pub public: &'a PubCryptoPublicKey,
    pub secret: &'a PubCryptoSecretKey,
}

pub fn encrypt_message(message: &str, keys: PubCryptoKeysRef, nonce: &Nonce) -> Vec<u8> {
    let byte_string_message = message.as_bytes();
    let ciphertext = box_::seal(byte_string_message, nonce, keys.public, keys.secret);
    ciphertext
}

pub fn decrypt_message(message: &Vec<u8>, keys: PubCryptoKeysRef, nonce: &Nonce) -> String {
    let byte_message = box_::open(message, &nonce, keys.public, keys.secret).unwrap();
    let message = str::from_utf8(&byte_message).unwrap();
    message.to_string()
}

type SignCryptoPublicKey = sodiumoxide::crypto::sign::PublicKey;
type SignCryptoSecretKey = sodiumoxide::crypto::sign::SecretKey;

pub struct SignKeys {
    pub secret: SignCryptoSecretKey,
    pub public: SignCryptoPublicKey,
}

pub fn generate_sign_keys() -> SignKeys {
    let (pk, sk) = sign::gen_keypair();
    SignKeys {
        secret: sk,
        public: pk,
    }
}

pub fn sign(data: &str, secret_key: &SignCryptoSecretKey) -> Vec<u8> {
    let byte_string_message = data.as_bytes();
    sign::sign(byte_string_message, secret_key)
}

pub fn verify(signed_data: &Vec<u8>, public_key: &SignCryptoPublicKey) -> Result<String, ()> {
    match sign::verify(signed_data, public_key) {
        Ok(vec) => {
            let message = str::from_utf8(&vec).unwrap();
            Ok(message.to_string())
        }
        Err(e) => Err(e),
    }
}

pub fn encrypt_message_anonymously(message: &str, key: &PubCryptoPublicKey) -> Vec<u8> {
    let byte_string_message = message.as_bytes();
    sealedbox::seal(byte_string_message, key)
}

pub fn unseal(sealed_message: &Vec<u8>, keys: PubCryptoKeysRef) -> Result<String, ()> {
    match sealedbox::open(sealed_message, keys.public, keys.secret) {
        Ok(vec) => {
            let message = str::from_utf8(&vec).unwrap();
            Ok(message.to_string())
        }
        Err(e) => Err(e),
    }
}

#[test]
fn public_key_communication() {
    let sender_keys = generate_keys();
    let receiver_keys = generate_keys();
    let nonce = generate_nonce();
    let message = "A secret message";

    let keys_to_send = PubCryptoKeysRef {
        secret: &sender_keys.secret,
        public: &receiver_keys.public,
    };
    let encrypted_message = encrypt_message(&message, keys_to_send, &nonce);

    let keys_to_receive = PubCryptoKeysRef {
        secret: &receiver_keys.secret,
        public: &sender_keys.public,
    };
    let decrypted_message = decrypt_message(&encrypted_message, keys_to_receive, &nonce);

    assert_eq!(message, decrypted_message);
}

#[test]
fn signing_message() {
    let keys = generate_sign_keys();
    let message = "Message to sign";
    let signed_message = sign(&message, &keys.secret);

    assert_eq!(message, verify(&signed_message, &keys.public).unwrap());
}

#[test]
fn anonymous_seal() {
    let keys_receiver = generate_keys();
    let message = "Message from a secret lover";
    let encrypted_message = encrypt_message_anonymously(&message, &keys_receiver.public);
    let keys_ref = PubCryptoKeysRef{ secret: &keys_receiver.secret, public: &keys_receiver.public };
    let decrypted_message = unseal(&encrypted_message, keys_ref).unwrap();

    assert_eq!(message, decrypted_message);
}
