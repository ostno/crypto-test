use std::str;
mod users;

fn main() {

    let bob = users::CryptoUser::new("Bob".into());
    let lucy = users::CryptoUser::new("Lucy".into());

    let message = "Hi Bob !";
    println!("\nLucy encrypts a message to Bob, using Bob's pub key : '{}'", message);

    let keys_for_lucy = crypto_adapter::PubCryptoKeysRef {
       secret: &lucy.keys.secret,
       public: &bob.keys.public
    };
    let nonce = &crypto_adapter::generate_nonce();

    let encrypted_message = crypto_adapter::encrypt_message(message, keys_for_lucy, nonce);
    match str::from_utf8(&encrypted_message) {
        Err(err) => {
            println!("\nEncrypted message is at least not a valid UTF8");
            println!("{}", err);
        },
        Ok(text) => {
            println!("Encrypted message is a valid UTF8 ? : {}", text);
        }
    }

    let keys_for_bob = crypto_adapter::PubCryptoKeysRef {
       secret: &bob.keys.secret,
       public: &lucy.keys.public
    };

    let decrypted_message = crypto_adapter::decrypt_message(&encrypted_message, keys_for_bob, nonce);

    println!("\nBob decrypts his message, using Lucy's pub key, and gets : '{}'", decrypted_message);


}
