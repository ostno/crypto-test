pub struct CryptoUser {
    pub name: String,
    pub keys: crypto_adapter::PubCryptoKeys,
}

impl CryptoUser {
    pub fn new(name: String) -> Self {
        CryptoUser {
            name: name,
            keys: crypto_adapter::generate_keys(),
        }
    }
}
